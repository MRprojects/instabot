


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

import java.io.*;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class VueStationMeteo extends JFrame  {
	// version 1
//	private JTextField fieldSemaine;
	//version 2
	private JComboBox fieldSemaine;
	private JTextField fieldPlace;
	private JTextField fieldvalpass;
	private JTextField fieldvalcom;
	private JTextField fieldValTemp;
	private JTextField fieldValPrec;
	private JTextField fieldValIter;
	private JTextField fieldValRatiocom;
	private JTextField fieldValRatioFollow;
	private JTextField fieldValRatioLike;
    private JTextArea jText;
	private JButton btnCharger;
	private JButton btnSauver;
	private JCheckBox btnLike;
	private JCheckBox btnFollow;
	private JButton btnJours[];
	private String nomFich;
	//ajout d'un bouton supprimer
	private JButton btnSupprimer;
	private String user;
	private String pass;
	private String url;
	private String hashtag;
	private String[] commentaire;
	private int iter;
	private int ratioFollow;
	private int ratioCommentaire;
	private int ratioLike;


	public VueStationMeteo(String titre){
		super(titre);
		this.initialise();
		this.initConnections();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.centreEcran(1200,400);
		this.setVisible(true);
	}

	public void centreEcran(int w, int h){
		Toolkit tk=Toolkit.getDefaultToolkit();
	    Dimension dim=tk.getScreenSize();
	    int X=(dim.width-w)/2;
	    int Y=(dim.height-h)/2;
	    this.setBounds(X,Y,w,h); //centre au milieu de la fenêtre
	}
	public void initialise(){
		this.setLayout(new BorderLayout());
		this.add(this.getPanelNord(),BorderLayout.NORTH);
		this.add(this.getPanelCentre(), BorderLayout.CENTER);
		this.add(this.getPanelWest(), BorderLayout.WEST);
		this.add(this.getPanelSud(),BorderLayout.SOUTH);
	}
	//définition des ecouteurs
	public void initConnections(){
		//gestion des boutons jour
		//btnJours[0].addActionListener(new GestBoutonsJours(0));
		//gestion des boutons sauver et charger
		this.btnCharger.addActionListener(new GestBoutonsJours(10));
		this.btnSauver.addActionListener(new GestBoutonsJours(11));
		//this.btnSupprimer.addActionListener(new GestBoutonsJours(12));

	}
	 //version 1
	public JPanel getPanelCentre(){
		JPanel pan = new JPanel();
		pan.setLayout(new BorderLayout());
		jText = new JTextArea(9,25);
		JScrollPane jsc = new JScrollPane(jText);
		pan.add(jsc,BorderLayout.CENTER);

		jText.setText("");
		return pan;
	}

	public JPanel getPanelNord(){
		//panel des boutons
		JPanel pan = new JPanel();
		//place
		//pan.setLayout(new GridLayout(10,2));

		JLabel lblPlace = new JLabel("login");
		pan.add(lblPlace);
		this.fieldPlace = new JTextField(10);
		pan.add(this.fieldPlace);
		//semaine
		JLabel lbSemaine = new JLabel("password");
		pan.add(lbSemaine);
		// version 1
//		this.fieldSemaine = new JTextField(5);
		//version 2
		Integer[] numSem = new Integer[52];
		for(int i=0; i<52;i++){
			//numSem[i]= new Integer(i+1); // si java version 1.3 par exemple
			numSem[i]= i+1;
		}
		//this.fieldSemaine = new JComboBox(numSem);
		//pan.add(this.fieldSemaine);
		this.fieldvalpass = new JTextField(10);
		pan.add(this.fieldvalpass);
		//temp
		JLabel lblValTemp= new JLabel("compte");
		pan.add(lblValTemp);
		fieldValTemp = new JTextField(10);
		pan.add(fieldValTemp);

		//precip
		JLabel lblValPrec= new JLabel("hashtag");
		pan.add(lblValPrec);
		fieldValPrec = new JTextField(7);
		pan.add(fieldValPrec);

		//commentaire
		JLabel lblvalcom= new JLabel("commentaire");
		pan.add(lblvalcom);
		fieldvalcom = new JTextField(30);
		pan.add(fieldvalcom);

		return pan;
	}



	public JPanel getPanelWest(){
		JPanel pan = new JPanel();
		//btnJours = new JButton[7];
		pan.setLayout(new GridLayout(7,1,10,10));
		/*for(int i=0; i< 7;i++){
			btnJours[i] = new JButton("salut");
			pan.add(btnJours[i]);
		}
		btnJours[0] = new JButton("salut");
		pan.add(btnJours[0]);*/
		//In initialization code:
	    btnLike = new JCheckBox("Like ?");
	    btnLike.setSelected(false);
	    pan.add(btnLike);

	    btnFollow = new JCheckBox("Follow ?");
	    btnFollow.setSelected(false);
	    pan.add(btnFollow);


		//itérations
		JLabel lblit= new JLabel("itérations");
		pan.add(lblit);
		fieldValIter= new JTextField(7);
		pan.add(fieldValIter);

		//ratio commentaire
		JLabel lblRatiocom= new JLabel("fréquence commentaires 1/n : n=");
		pan.add(lblRatiocom);
		fieldValRatiocom = new JTextField(7);
		pan.add(fieldValRatiocom);

		//ratio follow
		JLabel lblRatioFollow= new JLabel("fréquence follow 1/n : n=");
		pan.add(lblRatioFollow);
		fieldValRatioFollow = new JTextField(7);
		pan.add(fieldValRatioFollow);
		//ratio like
		JLabel lblRatioLike= new JLabel("fréquence like 1/n : n=");
		pan.add(lblRatioLike);
		fieldValRatioLike = new JTextField(7);
		pan.add(fieldValRatioLike);
		//ratio like
		JLabel stringcom= new JLabel("si plusieurs commentaires, les séparer par une virgule");
		pan.add(stringcom);

		return pan;
	}
	public JPanel getPanelSud(){
		JPanel panBout = new JPanel();
		JLabel lblBottom= new JLabel("rechercher par ");
		panBout.add(lblBottom);
		this.btnSauver = new JButton("tag");
		this.btnCharger = new JButton("compte");
		//this.btnSupprimer = new JButton("supprimer");
		panBout.add(this.btnCharger);
		panBout.add(this.btnSauver);
		//panBout.add(this.btnSupprimer);
		return panBout;

	}
	public void saisieValeurs(String jour){
		//afficher la place
		// afficher la semaine
		//String semaine = this.fieldSemaine.getSelectedItem().toString();
		String nbIter = this.fieldValIter.getText().toString();
		this.iter = Integer.parseInt(nbIter);
		if(!verifSaisies()){
			//pas de saisie
			return;
		}
				this.user = this.fieldPlace.getText().toString();

				this.hashtag = this.fieldValPrec.getText().toString();
				this.url = this.fieldValTemp.getText().toString();
				this.pass = this.fieldvalpass.getText().toString();
				this.commentaire = this.fieldvalcom.getText().toString().split(",");
				for(int i = 0; i < this.commentaire.length; i++)
				{
				  System.out.println("commentaire " + i +" ==> " + this.commentaire[i]);
				}
				String rCom = this.fieldValRatiocom.getText().toString();
				if(rCom.equals(""))
					this.ratioCommentaire=1;
				else
					this.ratioCommentaire = Integer.parseInt(rCom);
				String rFoll = this.fieldValRatioFollow.getText().toString();
				if (rFoll.equals(""))
					this.ratioFollow = 1;
				else
					this.ratioFollow = Integer.parseInt(rFoll);
				//System.out.println("ratio follow : "+ this.ratioFollow+" ratio com : "+this.ratioCommentaire);
				String rLike = this.fieldValRatioLike.getText().toString();

				if (rLike.equals(""))
					this.ratioLike = 1;
				else
					this.ratioLike = Integer.parseInt(rLike);

	}
	// vérification des saisies
	public boolean verifSaisies() {
		if((this.fieldPlace.getText().trim().equals("")|| this.fieldvalpass.getText().trim().equals("") ||
			this.fieldValPrec.getText().trim().equals(""))
			&& (this.fieldPlace.getText().trim().equals("")|| this.fieldvalpass.getText().trim().equals("") ||
			this.fieldValTemp.getText().trim().equals(""))){
			JOptionPane.showMessageDialog(this,"Veuillez remplir login, pass, hashtag ou login, pass, user!");
			return false;
		}
		if(this.iter<2){
			JOptionPane.showMessageDialog(this,"Veuillez remplir nombre d'itérations! (>2)");
			return false;
			}
		return true;
	}
	public void afficheValeurs(){
		this.jText.setText("user : "+this.user+" : passwd : "+this.pass+" : compte : "
		+this.url+" : hashtag : "+this.hashtag+" : commentaire : "
		+this.commentaire+" : iterations : "+this.iter);
	}
	public UntitledTestCase getValeurs(){
		UntitledTestCase launch = new UntitledTestCase();
		launch.setUser(this.user);
		launch.setPasswd(this.pass);
		launch.setURL(this.url);
		launch.setHashT(this.hashtag);
		if(!this.fieldvalcom.getText().trim().equals(""))
			launch.setComment(this.commentaire);
		launch.setBoolLike(btnLike.isSelected());
		launch.setBoolFollow(btnFollow.isSelected());
		launch.setIter(this.iter);
		launch.setRatioFollow(this.ratioFollow );
		launch.setRatioComment(this.ratioCommentaire );
		launch.setRatioLike(this.ratioLike );

		return launch;
	}

	//chargement d'un fichier
	public boolean charger(){

		return true;
	}
	//sauvegarde
	// il faut selectionner le texte à sauvegarder
	public boolean sauver(){
		//saisie du nom du fichier
		JFileChooser jfc = new JFileChooser(System.getProperty("user.dir"));
		jfc.setDialogTitle("Définir un fichier");
		jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		jfc.showSaveDialog(this);
		String fichier = jfc.getSelectedFile().getName();
		if(fichier.trim().equals("")){
			//nom du fichier incorrect
			return false;
		}
		//this.ensValTempPrec.sauve(fichier);
		return true;
	}
	//supprimer
	// il faut selectionner une ligne
	public boolean suppressionValeurs(){
		System.out.println("ayy");
		String selection = this.jText.getSelectedText();
        System.out.println("selection : "+selection);
		StringTokenizer stn = new StringTokenizer(selection,"=,]");
		stn.nextToken("=");
		String jour = stn.nextToken(",").substring(1).trim(); //il y a = au début
		stn.nextToken("=");
		String prec =stn.nextToken(",").substring(1).trim();
		stn.nextToken("=");
		String temp = stn.nextToken("]").substring(1).trim();
		System.out.println("VueStationMeteo.supprimer(): "+jour+" "+prec+" "+temp);
		float precF = Float.parseFloat(prec);
		float tempF = Float.parseFloat(temp);
		return true;

	}
	public static void main(String[] args) {
		 new VueStationMeteo("lourd 0.1.4 -- test iteration");



	}
/**************************************************************************************************
 * inner class
 *
 ************************************************************************************************/
  class GestBoutonsJours implements ActionListener{
	  private int id;
	  public GestBoutonsJours(int i){
		  this.id = i;
	  }
	@Override
	public void actionPerformed(ActionEvent e) {
		String jour = "salut";

		switch (id) {
		case 10:

			VueStationMeteo.this.saisieValeurs(jour);
			VueStationMeteo.this.afficheValeurs();

 			if(verifSaisies()){
 		 		UntitledTestCase launch = VueStationMeteo.this.getValeurs();
 				launch.setUp();
 				launch.testUntitledTestCase();
 			}
			break;
		case 11:

			VueStationMeteo.this.saisieValeurs(jour);
			VueStationMeteo.this.url="explore/tags/"+VueStationMeteo.this.hashtag;
			VueStationMeteo.this.afficheValeurs();

 			if(verifSaisies()){
 		 		UntitledTestCase launch2 = VueStationMeteo.this.getValeurs();
 				launch2.setUp();
    			launch2.testUntitledTestCase();
 			}
    		break;
		case 12:

			if(VueStationMeteo.this.suppressionValeurs()){
				VueStationMeteo.this.afficheValeurs();
			}
			break;
		default:
			VueStationMeteo.this.saisieValeurs(jour);
			VueStationMeteo.this.afficheValeurs();


			break;
		}

	}
  }




}
