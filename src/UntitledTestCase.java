
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Color;
import javax.swing.JFrame;

public class UntitledTestCase extends JFrame {

	  private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	  private String user;
	  private String password;
	  private String url;
	  private String hashtag;
	  private String[] comment = {""};
	  private boolean like = false;
	  private boolean follow = false;
	  private int iterations = 1;
	  private int ratioFollow;
	  private int ratioComment;
	  private int ratioLike;
	  private int exposantLike;
	  private int exposantComment;
	  private int exposantFollow;


	  public void setRatioLike(int n){
		  ratioLike = n;
		  int exposant=0;
		  while(n%10!=0){
			  n=n/10;
			  exposant++;
		  }
		  exposantLike = exposant;
	  }

	  public void setRatioFollow(int n){
		  ratioFollow = n;
		  int exposant=0;
		  while(n%10!=0){
			  n=n/10;
			  exposant++;
		  }
		  exposantFollow = exposant;
	  }

	  public void setRatioComment(int n){
		  ratioComment = n;
		  int exposant=0;
		  while(n%10!=0){
			  n=n/10;
			  exposant++;
		  }
		  exposantComment = exposant;
	  }

	  public void setIter(int n){
		  iterations = n;
	  }

	  public void setBoolFollow(boolean bf){
		  follow = bf;
	  }

	  public void setBoolLike(boolean bl){
		  like = bl;
	  }

	  public void setComment(String[] c){
		  comment= c;
	  }

	  public void setUser(String u){
		  user= u;
	  }

	  public void setPasswd(String p){
		  password=p;
	  }

	  public void setURL(String add){
		  url= add;
	  }
	  public void setHashT(String h){
		  hashtag= h;
	  }

	  public void setUp() {
			System.setProperty("webdriver.gecko.driver", "./chromedriver");
		    driver = new FirefoxDriver();
		    //driver = new ChromeDriver();
			baseUrl = "https://www.katalon.com/";
		    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		  }

	  public void testUntitledTestCase() {
		    driver.get("https://www.instagram.com/");
            driver.findElement(By.linkText("Connectez-vous")).click();
		    driver.findElement(By.linkText("Log in")).click();
		    driver.findElement(By.name("username")).clear();
		    driver.findElement(By.name("username")).sendKeys(user);
		    driver.findElement(By.name("password")).clear();
		    driver.findElement(By.name("password")).sendKeys(password);
		    driver.findElement(By.xpath("//span[@id='react-root']/section/main/article/div/div/div/form/span/button")).click();
		    try
		    {
		        Thread.sleep(8000);
		    }
		    catch(InterruptedException ex)
		    {
		        Thread.currentThread().interrupt();
		    }
		    driver.get("https://www.instagram.com/"+url);
		    //driver.get("https://www.instagram.com/explore/tags"+hashtag);
		    driver.findElement(By.xpath("//span[@id='react-root']/section/main/article/div/div/div/div/a/div")).click();
			System.out.println("ratio follow : "+ this.ratioFollow+" ratio com : "+this.ratioComment+" ratio like : "+this.ratioLike);
		    for(int i=1; i<=iterations; i++){
		    	System.out.println("début iteration "+i);
			    if(like && driver.findElement(By.xpath("//section/a/span")).getText().equals("Like") && (exposantLike*i)%ratioLike==0){// dejalike ?
						System.out.println("like : "+like);
			    		driver.findElement(By.xpath("//section/a/span")).click(); // photo like
			    }
			    if(!comment.equals("") && (exposantComment*i)%ratioComment==0){
			    	int numCom=(10*i)%comment.length;
					System.out.println("comment : "+comment[numCom]);
				    driver.findElement(By.xpath("//textarea")).clear();
			    	driver.findElement(By.xpath("//textarea")).sendKeys(comment[numCom]+"\n"); // commente
				    try
				    {
				        Thread.sleep(8000);
				    }
				    catch(InterruptedException ex)
				    {
				        Thread.currentThread().interrupt();
				    }
			    }
			    if(follow && driver.findElement(By.xpath("//span/button")).getText().equals("Follow") && (exposantFollow*i)%ratioFollow==0){
					System.out.println("follow : "+follow);
			    	driver.findElement(By.xpath("//span/button")).click();
				//driver.findElement(By.xpath("//div[2]/span[2]/button")).click(); // follow sur page user
				    try
				    {
				        Thread.sleep(2000);
				    }
				    catch(InterruptedException ex)
				    {
				        Thread.currentThread().interrupt();
				    }
			    }
			    driver.findElement(By.linkText("Next")).click(); // photo next
			    try
			    {
			        Thread.sleep(1000);
			    }
			    catch(InterruptedException ex)
			    {
			        Thread.currentThread().interrupt();
			    }
		    	System.out.println("fin iteration "+i);

			 }

	  }
  public static void main(String[] args) throws Exception {

//	  JFrame fen = new JFrame("yo");
	//	Toolkit atoolkit = Toolkit.getDefaultToolkit();

	//	fen.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	//	fen.setBounds(100, 100,atoolkit.getScreenSize().width/2,atoolkit.getScreenSize().height/2);
	//	fen.setVisible(true);

	  UntitledTestCase test = new UntitledTestCase();
	  test.setUp();
	  test.setURL("explore/tags/url");
	  test.setUser("USERNAME");
	  test.setPasswd("PASSWD");
	  test.testUntitledTestCase();

  }
}
